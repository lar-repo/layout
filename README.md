# Documentation of Layout v 2.0.*

* **0. About**
* **1. Quick start**
* **2. Configs**
* **3. Layout**
* **4. Component**
* **5. Vue** (more info in LJS package)
* **6. Commands**
* **7. Blade directives**
* **8. Respond** (only method desc, all doc in LJS package)
* **9. JAX Executors**
* **10. Middleware**
* **11. Extends**
* **12. Languages** (in development)
* **13. Examples**

## 0. About

Это пакет предназначен для того что бы легко и быстро собрать динамический шаблон из любой верстки. А так-же пакет
обьеденяет в себе использование `Laravel Blade` и `VueJs`
для достижения большей эфективности шаблона (опционально)
